# El Baraton

Rappi frontend code challenge, create a virtual store

Clone this repo (`git clone https://gitlab.com/chuckbot/baraton-app.git`)
 
 NODE_VERSION : 10.13.0
 
```bash
 cd baraton-app
 npm install
 npm run build
 npm run start  //http://localhost:3000
 ```